package br.com.releasesolutions;

import javax.swing.*;

// Figura 12.18: CheckBoxTest.java
// Testando br.com.releasesolutions.CheckBoxFrame
public class CheckBoxTest {

    public static void main(String[] args) {

        System.out.println();
        CheckBoxFrame checkBoxFrame = new CheckBoxFrame();
        checkBoxFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        checkBoxFrame.setSize(275, 100);
        checkBoxFrame.setVisible(true);
    }
} // fim da classe CheckBoxTest

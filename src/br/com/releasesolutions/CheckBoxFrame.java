package br.com.releasesolutions;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

// Figura 12.17: br.com.releasesolutions.CheckBoxFrame.java
// JCheckBoxes e eventos de item.
public class CheckBoxFrame extends JFrame {

    private final JTextField textField; // exibe o texto na altera��o de fontes
    private final JCheckBox boldCheckBox; // para marcar/desmarcar estilo negrito
    private final JCheckBox italicCheckBox; // para marcar/desmarcar estilo it�lico


    public CheckBoxFrame() {
        super("JCheckBox Test");
        setLayout(new FlowLayout());

        // configura JTextField e sua fonte
        textField = new JTextField("Watch the font style change", 20);
        textField.setFont(new Font("Serif", Font.PLAIN, 14));
        add(textField); // adiciona textField ao JFrame

        boldCheckBox = new JCheckBox("Bold");
        italicCheckBox = new JCheckBox("Italic");
        add(boldCheckBox); // adiciona caixa de sele��o de estilo negrito ao JFrame
        add(italicCheckBox); // adiciona caixa de sele��o de estilo it�lico ao JFrame

        // listeners registradores para JCheckBoxes
        CheckBoxHandler handler = new CheckBoxHandler();
        boldCheckBox.addItemListener(handler);
        italicCheckBox.addItemListener(handler);
    }

    private class CheckBoxHandler implements ItemListener {

        // classe interna private para tratamento de evento ItemListener
        @Override
        public void itemStateChanged(ItemEvent event) {

            Font font = null; // armazena a nova Font

            // determina quais CheckBoxes est�o marcadas e cria Font
            if (boldCheckBox.isSelected() && italicCheckBox.isSelected())
                font = new Font("Serif", Font.BOLD + Font.ITALIC, 14);
            else if (boldCheckBox.isSelected())
                font = new Font("Serif", Font.BOLD, 14);
            else if (italicCheckBox.isSelected())
                font = new Font("Serif", Font.ITALIC, 14);
            else
                font = new Font("Serif", Font.PLAIN, 14);

            textField.setFont(font);
        }
    }
} // fim da classe br.com.releasesolutions.CheckBoxFrame
